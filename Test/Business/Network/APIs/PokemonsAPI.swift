import Foundation
import Moya

enum PokemonsAPI {
    case getPokemons(getPokemonsRequest: PokemonsRequest)
    case getPokemon(getPokemonRequest: PokemonRequest)
}

extension PokemonsAPI: TargetType {
    var baseURL: URL { AppConstant.apiUrl }
    
    var path: String {
        switch self {
        case .getPokemons: return "/pokemon"
        case .getPokemon(let getPokemonRequest): return "/pokemon/\(getPokemonRequest.name)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getPokemons, .getPokemon: return .get
        }
    }
    
    var sampleData: Data { Data() }
    
    var task: Task {
        switch self {
        case .getPokemons(let getPokemonsRequest):
            let urlEncoding = URLEncoding(arrayEncoding: .noBrackets, boolEncoding: .literal)
            return .requestParameters(parameters: getPokemonsRequest.dictionary, encoding: urlEncoding)
        case .getPokemon:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? { ["Content-type": "application/json"] }
}
