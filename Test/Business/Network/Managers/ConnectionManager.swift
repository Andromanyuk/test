import Foundation
import Network

protocol ConnectionManagerProtocol {
    var isConnected: Bool { get }
    var isExpensive: Bool { get }
    var interfaceType: NWInterface.InterfaceType? { get }
    var availableInterfacesTypes: [NWInterface.InterfaceType]? { get }
    var didStartMonitoringHandler: (() -> Void)? { get set }
    var didStopMonitoringHandler: (() -> Void)? { get set }
    var networkStatusChangeHandler: ((Bool) -> Void)? { get set }
    
    func startMonitoring()
    func stopMonitoring()
}

final class ConnectionManager: ConnectionManagerProtocol {
    private var monitor: NWPathMonitor?
    
    var isMonitoring = false
    
    var didStartMonitoringHandler: (() -> Void)?
    var didStopMonitoringHandler: (() -> Void)?
    var networkStatusChangeHandler: ((Bool) -> Void)?
    
    var isConnected: Bool {
        guard let monitor = monitor else { return false }
        return monitor.currentPath.status == .satisfied
    }
    
    var interfaceType: NWInterface.InterfaceType? {
        guard let monitor = monitor else { return nil }
        
        return monitor.currentPath.availableInterfaces.filter {
            monitor.currentPath.usesInterfaceType($0.type) }.first?.type
    }
    
    var availableInterfacesTypes: [NWInterface.InterfaceType]? {
        guard let monitor = monitor else { return nil }
        return monitor.currentPath.availableInterfaces.map { $0.type }
    }
    
    var isExpensive: Bool {
        monitor?.currentPath.isExpensive ?? false
    }

    deinit {
        stopMonitoring()
    }

    func startMonitoring() {
        guard !isMonitoring else { return }
        
        monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "NetStatus_Monitor")
        monitor?.start(queue: queue)
        
        monitor?.pathUpdateHandler = { [weak self] _ in
            self?.networkStatusChangeHandler?(self?.isConnected ?? false)
        }
        
        isMonitoring = true
        didStartMonitoringHandler?()
    }
    
    func stopMonitoring() {
        guard isMonitoring, let monitor = monitor else { return }
        monitor.cancel()
        self.monitor = nil
        isMonitoring = false
        didStopMonitoringHandler?()
    }
}
