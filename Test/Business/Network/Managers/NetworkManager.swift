import Moya
import Foundation

protocol NetworkManagerProtocol {
    func request<T: TargetType, U: Decodable> (_ target: T, model: U.Type,
                                               completion: ((Result<U, ApiError>) -> ())?)
}

struct NetworkManager: NetworkManagerProtocol {
    private let provider: MoyaProvider<MultiTarget>
    private let decoder: JSONDecoder
    private let connectionManager: ConnectionManagerProtocol
    
    init(
        provider: MoyaProvider<MultiTarget>? = nil,
        connectionManager: ConnectionManagerProtocol
    ) {
        self.provider = MoyaProvider<MultiTarget>()
        self.connectionManager = connectionManager
        self.connectionManager.startMonitoring()
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        self.decoder = decoder
    }
    
    func request<T: TargetType, U: Decodable> (_ target: T, model: U.Type,
                                               completion: ((Result<U, ApiError>) -> ())? = nil) {
        if !connectionManager.isConnected {
            completion?(.failure(ApiError.noInternetConnection))
            return
        }
        provider.request(MultiTarget(target)) { result in
            switch result {
            case .success(let response):
                switch response.statusCode {
                case (200...300):
                    do {
                        let decodableModel = try response.map(model)
                        completion?(.success(decodableModel))
                    } catch(let error) {
                        print(error)
                        completion?(.failure(ApiError.badResponseFormat))
                    }
                default:
                    completion?(.failure(ApiError.generalError))
                }
            case .failure(let error):
                print(error)
                completion? (.failure(ApiError.generalError))
            }
        }
    }
}
