import Foundation

// MARK: ServiceProtocol
protocol PokemonsServiceProtocol: ServiceProtocol {
    func getPokemons(
        pokemonsRequest: PokemonsRequest,
        completion: @escaping (PokemonsResponse) -> Void,
        errorHandler: ((ApiError) -> Void)?
    )
    func getPokemon(
        pokemonsRequest: PokemonRequest,
        completion: @escaping (PokemonResponse) -> Void,
        errorHandler: ((ApiError) -> Void)?
    )
}

final class PokemonsService: PokemonsServiceProtocol {
    // MARK: Properties
    
    typealias Request = PokemonsAPI
    var request: Request?
    
    private let networkManager: NetworkManagerProtocol
    
    init(with networkManager: NetworkManagerProtocol) {
        self.networkManager = networkManager
    }
    
    // MARK: PokemonsServiceProtocol
    
    func getPokemons(
        pokemonsRequest: PokemonsRequest,
        completion: @escaping (PokemonsResponse) -> Void,
        errorHandler: ((ApiError) -> Void)? = nil
    ) {
        request = Request.getPokemons(getPokemonsRequest: pokemonsRequest)
        guard let request = request else { return }
        
        networkManager.request(request, model: PokemonsResponse.self, completion: { result in
            switch result {
            case .success(let response):
                completion(response)
            case .failure(let error):
                errorHandler?(error)
            }
        })
    }
    
    func getPokemon(
        pokemonsRequest: PokemonRequest,
        completion: @escaping (PokemonResponse) -> Void,
        errorHandler: ((ApiError) -> Void)? = nil
    ) {
        request = Request.getPokemon(getPokemonRequest: pokemonsRequest)
        guard let request = request else { return }
        
        networkManager.request(request, model: PokemonResponse.self, completion: { result in
            switch result {
            case .success(let response):
                completion(response)
            case .failure(let error):
                errorHandler?(error)
            }
        })
    }
    
    // MARK: ServiceProtocol
    
    func clear() {
        request = nil
    }
}
