import Foundation

protocol ServiceProtocol {
    func clear()
}
protocol ServiceLocatorProtocol {
    func service<T>() -> T?
}

// MARK: ServiceLocatorProtocol
final class ServiceLocator: ServiceLocatorProtocol {
    private lazy var services: [String: Any] = [:]
    
    init<T>(with services: [T]) {
        addServices(services)
    }
    
    func addService<T>(_ service: T) {
        let key = typeName(T.self)
        services[key] = service
    }
    
    func addServices<T>(_ services: [T]) {
        services.forEach { addService($0) }
    }
    
    func removeService<T>(_ service: T) {
        let key = typeName(T.self)
        services.removeValue(forKey: key)
    }
    
    func clear() {
        services.removeAll()
    }
    
    func service<T>() -> T? {
        let key = typeName(T.self)
        return services[key] as? T
    }
}

// MARK: Private
private extension ServiceLocator {
    func typeName(_ some: Any) -> String {
        (some is Any.Type) ? "\(some)" : "\(type(of: some))"
    }
}
