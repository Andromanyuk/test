import Foundation

struct PokemonsRequest: Encodable {
    let offset: Int
    let limit: Int
    
    internal init(offset: Int = .zero, limit: Int) {
        self.offset = offset
        self.limit = limit
    }
}
