import Foundation

struct PokemonResponse: Decodable {
    let id: Int?
    let name: String?
    let sprites: Sprite
    let types: [PokemonType]?
    let height: Double?
    let weight: Double?
}

struct PokemonType: Decodable {
    let slot: Int?
    let type: PokemonTypeInfo?
}

struct PokemonTypeInfo: Decodable {
    let name: String?
    let url: String?
}

struct Sprite: Decodable {
    let imageUrl: String?
    
    enum CodingKeys: String, CodingKey {
      case imageUrl = "front_default"
    }
}
