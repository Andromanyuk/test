import Foundation

struct PokemonRequest: Encodable {
    let name: String
}
