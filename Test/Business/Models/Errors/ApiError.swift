import Foundation

enum ApiError: Error {
    case generalError
    case noInternetConnection
    case badResponseFormat
    
    var localizedDescription: String? {
        switch self {
        case .generalError:             return "Something was wrong.\nPlease try again later"
        case .noInternetConnection:     return "Check your internet connection and try again"
        case .badResponseFormat:        return "Bad response format"
        }
    }
}
