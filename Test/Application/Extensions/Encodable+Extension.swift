import Foundation

extension Encodable {
    var dictionary: [String: Any] {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(Date().encodingFormatter)
        return (try? JSONSerialization.jsonObject(with: encoder.encode(self))) as? [String: Any] ?? [:]
    }
}
