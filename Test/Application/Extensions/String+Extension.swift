import Foundation

extension String {
    var fromResponseToCommonFormat: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Date.httpResponseFormat
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: self)
        return date
    }
    
    var capitalizingFirstLetter: String { prefix(1).capitalized + dropFirst() }
}
