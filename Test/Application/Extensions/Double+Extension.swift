import Foundation

extension Double {
    func multiplied(by multiplier: Double) -> Double {
        self * multiplier
    }
}
