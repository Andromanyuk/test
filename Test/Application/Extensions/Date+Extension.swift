import Foundation

extension Date {
    static let commonFormat = "yyyy-MM-dd HH:mm:ss VV"
    static let httpResponseFormat = "E, dd MMM yyyy HH:mm:ss ZZZ"
    
    static var commonFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.timeZone = TimeZone(secondsFromGMT: .zero)
        formatter.locale = Locale.current
        return formatter
    }
    
    var encodingFormatter: DateFormatter {
        let encodingFormatter = Date.commonFormatter
        encodingFormatter.dateFormat = Date.commonFormat
        return encodingFormatter
    }
}
