import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let connectionManager = ConnectionManager()
        let networkManager = NetworkManager(connectionManager: connectionManager)
        let serviceLocator = ServiceLocator(with: [PokemonsService(with: networkManager)])
        
        let navigationVC = BaseNavigationVC()
        
        let pokemonsListInput = PokemonsListInput(serviceLocator: serviceLocator, navigationHandler: navigationVC)
        let pokemonListComposer = PokemonsListComposer.assemble(withInput: pokemonsListInput)
        
        navigationVC.setViewControllers([pokemonListComposer.viewController], animated: false)
        
        window?.rootViewController = navigationVC
        window?.makeKeyAndVisible()
        
        return true
    }
}

