import Foundation

enum AppConstant {
    static let apiUrl = URL(string: "https://pokeapi.co/api/v2")!
}
