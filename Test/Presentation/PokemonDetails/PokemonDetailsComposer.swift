import Foundation
import UIKit

final class PokemonDetailsComposer: BaseComposerProtocol {
    let viewController: UIViewController
    
    class func assemble(withInput input: PokemonDetailsInput) -> PokemonDetailsComposer {
        let presenter = PokemonDetailsPresenter(
            with: input.serviceLocator,
            navigationHandler: input.navigationHandler,
            pokemonName: input.pokemonName
        )
        let viewController = PokemonDetailsVC(with: presenter)
        presenter.view = viewController
        return PokemonDetailsComposer(viewController: viewController)
    }
    
    private init(viewController: PokemonDetailsVC) {
      self.viewController = viewController
    }
}

struct PokemonDetailsInput {
    let serviceLocator: ServiceLocatorProtocol
    let navigationHandler: NavigationProtocol
    let pokemonName: String
}

protocol PokemonDetailsViewInput: AnyObject {
    func update(with model: PokemonDetailsProtocol)
}

protocol PokemonDetailsViewOutput: AnyObject {
    func viewDidLoad()
}
