import UIKit

final class PokemonDetailsVC: UIViewController {
    // MARK: Constants
    
    private enum Constant {
        static let unknownPlaceholderString = "unknown"
    }
    
    // MARK: Outlets
    
    @IBOutlet private weak var infoContainerView: UIView!
    @IBOutlet private weak var pokemonImageVIew: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var typesLabel: UILabel!
    @IBOutlet private weak var weightLabel: UILabel!
    @IBOutlet private weak var heightLabel: UILabel!
    
    // MARK: Properties
    
    private let output: PokemonDetailsViewOutput
    
    // MARK: Lifecycle
    
    init(with output: PokemonDetailsViewOutput) {
        self.output = output
        let bundle = Bundle(for: type(of: self))
        super.init(nibName: R.nib.pokemonDetailsView.name, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
    }
}

// MARK: PokemonDetailsViewInput
extension PokemonDetailsVC: PokemonDetailsViewInput {
    func update(with model: PokemonDetailsProtocol) {
        infoContainerView.isHidden = false
        nameLabel.text = model.name
        let types = model.types?.joined(separator: ", ")
        typesLabel.text = "Types: \(types ?? Constant.unknownPlaceholderString)"
        weightLabel.text = "Weight(kg): \(model.weight?.description ?? Constant.unknownPlaceholderString)"
        heightLabel.text = "Height(cm): \(model.height?.description ?? Constant.unknownPlaceholderString)"
        
        guard let imageUrl = model.imageUrl,
              let urlToLoad = URL(string: imageUrl),
              let imageData = try? Data(contentsOf: urlToLoad)
        else { return }
        pokemonImageVIew.image = UIImage(data: imageData)
    }
}
