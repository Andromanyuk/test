import Foundation

final class PokemonDetailsPresenter: BasePresenter {
    // MARK: Constants
    
    private enum Constant {
        static let weightMultiplier = 0.1
        static let heightMultiplier = 10.0
    }
    
    // MARK: Properties
    
    weak var view: PokemonDetailsViewInput!
    
    private let pokemonsService: PokemonsService?
    private let pokemonName: String
    
    // MARK: Lifecycle
    
    init(
        with serviceLocator: ServiceLocatorProtocol,
        navigationHandler: NavigationProtocol,
        pokemonName: String
    ) {
        pokemonsService = serviceLocator.service()
        self.pokemonName = pokemonName
        
        super.init(with: navigationHandler)
    }
}

// MARK: PokemonDetailsViewOutput
extension PokemonDetailsPresenter: PokemonDetailsViewOutput {
    func viewDidLoad() {
        getPokemonDetails(by: pokemonName)
    }
}

// MARK: Private 
private extension PokemonDetailsPresenter {
    func getPokemonDetails(by name: String) {
        let pokemonRequest = PokemonRequest(name: name)
        
        pokemonsService?.getPokemon(
            pokemonsRequest: pokemonRequest,
            completion: { [weak self] response in
                let pokemonDetailsModel = PokemonDetailsModel(
                    name: response.name?.capitalized,
                    imageUrl: response.sprites.imageUrl,
                    types: response.types?.compactMap { $0.type?.name },
                    weight: response.weight?.multiplied(by: Constant.weightMultiplier),
                    height: response.height?.multiplied(by: Constant.heightMultiplier)
                )
                self?.view.update(with: pokemonDetailsModel)
            },
            errorHandler: { error in
                print((error as ApiError).localizedDescription)
            }
        )
    }
}
