import Foundation
import UIKit

protocol PokemonDetailsProtocol {
    var name: String? { get }
    var imageUrl: String? { get }
    var types: [String]? { get }
    var weight: Double? { get }
    var height: Double? { get }
}

struct PokemonDetailsModel: PokemonDetailsProtocol {
    let name: String?
    let imageUrl: String?
    let types: [String]?
    let weight: Double?
    let height: Double?
}
