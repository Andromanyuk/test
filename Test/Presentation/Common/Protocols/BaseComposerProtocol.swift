import Foundation
import UIKit

protocol BaseComposerProtocol {
    var viewController: UIViewController { get }
}
