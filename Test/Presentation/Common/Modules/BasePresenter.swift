import Foundation

protocol BasePresenterProtocol {
    var navigationHandler: NavigationProtocol! { get }
}

class BasePresenter: BasePresenterProtocol {
    weak var navigationHandler: NavigationProtocol!
    
    init(with navigationHandler: NavigationProtocol) {
        self.navigationHandler = navigationHandler
    }
}
