import Foundation
import UIKit

protocol NavigationProtocol: AnyObject {
    func pushVC(of composer: BaseComposerProtocol, animated: Bool)
}

final class BaseNavigationVC: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.tintColor = .black
    }
}

extension BaseNavigationVC: NavigationProtocol {
    func pushVC(of composer: BaseComposerProtocol, animated: Bool) {
        pushViewController(composer.viewController, animated: animated)
    }
}
