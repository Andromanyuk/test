import Foundation
import UIKit

final class PokemonsListComposer: BaseComposerProtocol {
    let viewController: UIViewController
    
    class func assemble(withInput input: PokemonsListInput) -> PokemonsListComposer {
        let presenter = PokemonsListPresenter(with: input.serviceLocator, navigationHandler: input.navigationHandler)
        let viewController = PokemonsListVC(with: presenter)
        presenter.view = viewController
        return PokemonsListComposer(viewController: viewController)
    }
    
    private init(viewController: PokemonsListVC) {
      self.viewController = viewController
    }
}

struct PokemonsListInput {
    let serviceLocator: ServiceLocatorProtocol
    let navigationHandler: NavigationProtocol
}

protocol PokemonsListViewInput: AnyObject {
    func update()
}

protocol PokemonsListViewOutput: AnyObject {
    var titles: [PokemonCellModelProtocol] { get }
    
    func viewDidLoad()
    func didSelectPokemon(at index: Int)
}
