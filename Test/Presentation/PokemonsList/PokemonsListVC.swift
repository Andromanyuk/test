import Foundation
import UIKit

final class PokemonsListVC: UIViewController {
    // MARK: Constants
    
    private enum Constant {
        static let tableViewBottomInset: CGFloat = 10
        static let cellHeight: CGFloat = 50
        static let title = "Pokemons"
    }
    
    // MARK: Outlets
    
    @IBOutlet private weak var pokemonsTableView: UITableView!
    
    // MARK: Properties
    
    private let output: PokemonsListViewOutput
    
    // MARK: Lifecycle
    
    init(with output: PokemonsListViewOutput) {
        self.output = output
        let bundle = Bundle(for: type(of: self))
        super.init(nibName: R.nib.pokemonsListView.name, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        output.viewDidLoad()
    }
}

// MARK: Private
private extension PokemonsListVC {
    private func setupUI() {
        pokemonsTableView.register(R.nib.pokemonTableCell)
        pokemonsTableView.contentInset.bottom = Constant.tableViewBottomInset
        
        title = Constant.title
    }
}

// MARK: PokemonsListViewInput
extension PokemonsListVC: PokemonsListViewInput {
    func update() {
        pokemonsTableView.reloadData()
    }
}

// MARK: UITableViewDataSource
extension PokemonsListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        output.titles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.pokemonTableCell, for: indexPath)!
        cell.configure(with: output.titles[indexPath.row])
        return cell
    }
}

// MARK: UITableViewDelegate
extension PokemonsListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.didSelectPokemon(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constant.cellHeight
    }
}
