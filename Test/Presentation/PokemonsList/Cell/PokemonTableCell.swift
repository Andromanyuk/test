import UIKit

// MARK: Protocols
protocol PokemonCellModelProtocol {
    var title: String { get }
}

final class PokemonTableCell: UITableViewCell {
    // MARK: Outlets
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: Methods
    
    func configure(with model: PokemonCellModelProtocol) {
        titleLabel.text = model.title.capitalized
    }
}
