import Foundation

final class PokemonsListPresenter: BasePresenter {
    // MARK: Constants
    
    private enum Constant {
        static let pokemonsToLoadLimit = 20
    }
    
    // MARK: Properties
    
    weak var view: PokemonsListViewInput!
    
    private let serviceLocator: ServiceLocatorProtocol
    private let pokemonsService: PokemonsService?
    
    private var page: Int = .zero
    private var pokemons: [Pokemon] = []
    
    // MARK: Lifecycle
    
    init(with serviceLocator: ServiceLocatorProtocol, navigationHandler: NavigationProtocol) {
        pokemonsService = serviceLocator.service()
        self.serviceLocator = serviceLocator
        
        super.init(with: navigationHandler)
    }
}

// MARK: PokemonsListViewOutput
extension PokemonsListPresenter: PokemonsListViewOutput {
    var titles: [PokemonCellModelProtocol] {
        pokemons.compactMap {
            guard let title = $0.name else { return nil }
            return PokemonCellModel(title: title)
        }
    }
    
    func viewDidLoad() {
        getPokemons()
    }
    
    func didSelectPokemon(at index: Int) {
        guard let name = pokemons[index].name else { return }
        
        pushToPokemonDetails(with: name)
    }
}

// MARK: Private
private extension PokemonsListPresenter {
    func getPokemons(paginate: Bool = false) {
        page = paginate ? (page + 1) : .zero
        
        let limit = Constant.pokemonsToLoadLimit
        let offset = page * limit
        let pokemonsRequest = PokemonsRequest(offset: offset, limit: limit)
        
        pokemonsService?.getPokemons(
            pokemonsRequest: pokemonsRequest,
            completion: { [weak self] response in
                self?.pokemons = response.results ?? []
                self?.view.update()
            },
            errorHandler: { error in
                print((error as ApiError).localizedDescription)
            }
        )
    }
    
    func pushToPokemonDetails(with name: String) {
        let pokemonDetailsInput = PokemonDetailsInput(
            serviceLocator: serviceLocator,
            navigationHandler: navigationHandler,
            pokemonName: name
        )
        let pokemonDetailsComposer = PokemonDetailsComposer.assemble(withInput: pokemonDetailsInput)
        navigationHandler.pushVC(of: pokemonDetailsComposer, animated: true)
    }
}
